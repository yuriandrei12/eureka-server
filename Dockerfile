FROM openjdk:8-jdk-alpine

ARG JAR_FILE=target/eureka-0.0.1.jar

WORKDIR /opt/apps/eureka

COPY ${JAR_FILE} eureka.jar

ENTRYPOINT ["java","-jar","eureka.jar"]

EXPOSE 8761/tcp