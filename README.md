Para apontar para o Eureka:

####**.properties**

````
eureka.instance.hostname: http://18.224.55.106:8761
eureka.client.service-url.defaultZone: http://18.224.55.106:8761/eureka
````
 
####**.yml**
````
eureka:
    client:
        service-url:
            defaultZone: http://18.224.55.106:8761/eureka
    instance:
        hostname: http://18.224.55.106:8761
````